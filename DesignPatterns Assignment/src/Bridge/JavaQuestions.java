/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bridge;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author SUNNY
 */
public class JavaQuestions implements Question{
    private List <String> questions = new ArrayList<String>();
    private int current = 0;  
    
    public JavaQuestions(){  
        questions.add("What is class? ");  
        questions.add("What is object? ");  
    }  
    
    @Override
    public void nextQuestion() {
        
    }

    @Override
    public void previousQuestion() {
        
    }

    @Override
    public void newQuestion(String q) {
        questions.add(q);
    }

    @Override
    public void deleteQuestion(String q) {
        
    }

    @Override
    public void displayQuestion() {
        
    }

    @Override
    public void displayAllQuestions() {
        for (String quest : questions) {  
            System.out.println(quest);  
        }
    }
    
}
