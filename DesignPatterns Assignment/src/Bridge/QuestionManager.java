/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bridge;

/**
 *
 * @author SUNNY
 */
public class QuestionManager { 
    public Question q;  
    public String catalog;  
    
    public QuestionManager(String catalog) {  
        this.catalog=catalog;  
    }  
    public void next() {  
        q.nextQuestion();  
    }  
    public void previous() {  
        q.previousQuestion();  
    }  
    public void newOne(String quest) {  
        q.newQuestion(quest);  
    }  
    public void delete(String quest) {  
        q.deleteQuestion(quest);  
    }  
    public void display() {  
        q.displayQuestion();  
    }  
    public void displayAll() {  
        System.out.println("Question Paper: " + catalog);  
        q.displayAllQuestions();  
    }  
}
