/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractFactory;

/**
 *
 * @author SUNNY
 */
public class FactoryCreator {
    public static AbstractFactory getFactory(String choice){
        if(choice.equals("Bank")){
            return new BankFactory();
        }
        else if(choice.equals("Loan")){
            return new LoanFactory();
        }
        return null;
    }
}
