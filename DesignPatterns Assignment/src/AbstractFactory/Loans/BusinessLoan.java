/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractFactory.Loans;

import AbstractFactory.Loan;

/**
 *
 * @author SUNNY
 */
public class BusinessLoan extends Loan {

    @Override
    public void getInterestRate(double rate) {
        System.out.println("BusinessLoan interest rate: "+rate);
    }
    
}
