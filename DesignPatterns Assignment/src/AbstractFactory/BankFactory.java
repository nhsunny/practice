/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractFactory;

import AbstractFactory.Banks.HDFC;
import AbstractFactory.Banks.SBI;
import AbstractFactory.Banks.ICICI;

/**
 *
 * @author SUNNY
 */
public class BankFactory extends AbstractFactory{  
    public Bank getBank(String bank){  
       if(bank == null){  
          return null;  
       }  
       if(bank.equals("HDFC")){  
          return new HDFC();  
       } 
       else if(bank.equals("ICICI")){  
          return new ICICI();  
       } 
       else if(bank.equals("SBI")){  
          return new SBI();  
       }  
       return null;  
    } 
    
    @Override
    public Loan getLoan(String loan) {  
        return null;  
    }
}
