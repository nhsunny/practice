/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractFactory.Banks;

import AbstractFactory.Bank;

/**
 *
 * @author SUNNY
 */
public class ICICI implements Bank { 
    private String BNAME; 
    
    public ICICI(){  
        BNAME="ICICI BANK";  
    }  
    
    @Override
     public String getBankName() {  
        return BNAME;  
    }  
}
