package designpatterns;

/**
 *
 * @author S.M. Nabid Hasan Sunny 2014200000021
 */

import AbstractFactory.AbstractFactory;
import AbstractFactory.Bank;
import AbstractFactory.FactoryCreator;
import AbstractFactory.Loan;
import Adapter.BankCustomer;
import Adapter.CreditCard;
import Bridge.JavaQuestions;
import Bridge.QuestionFormat;
import Decorator.ChineeseFood;
import Decorator.Food;
import Decorator.NonVegFood;
import Decorator.VegFood;
import Factory.GetPlanFactory;
import Factory.Plan;
import Iterator.CollectionofNames;
import Iterator.Iterator;

public class DesignPatterns {
    public static void main(String[] args) {
        //Factory Pattern
        System.out.println("___FACTORY PATTERN___\n");
        
        GetPlanFactory planFactory = new GetPlanFactory();
        
        Plan dp = planFactory.getPlan("domestic");
        Plan cp = planFactory.getPlan("commercial");
        Plan ip = planFactory.getPlan("institutional");
        
        dp.getRate();
        cp.getRate();
        ip.getRate();
        
        //Abstract Factory Pattern
        System.out.println("\n___ABSTRACT FACTORY PATTERN___\n");
        
        AbstractFactory bankFactory = FactoryCreator.getFactory("Bank");
        AbstractFactory loanFactory = FactoryCreator.getFactory("Loan");
        
        Bank hdfc = bankFactory.getBank("HDFC");
        Loan homeLoan = loanFactory.getLoan("Home");
        
        System.out.println(hdfc.getBankName());
        homeLoan.getInterestRate(10);
        
        //Adapter Pattern
        System.out.println("\n___ADAPTER PATTERN___\n");
        
        CreditCard creditCard = new BankCustomer();
        creditCard.giveBankDetails();
        System.out.println(creditCard.getCreditCard());
        
        //Singleton Pattern
        System.out.println("\n___SINGLETON PATTERN___\n");
        
        Singleton.getInstance().demoFunction();
        
        //Bridge Pattern
        System.out.println("\n___BRIDGE PATTERN___\n");
          
        QuestionFormat questions = new QuestionFormat("Java Programming Language");  
        questions.q = new JavaQuestions();   
        questions.newOne("What is inheritance? ");  
        questions.displayAll();  
        
        //Decorator Pattern
        System.out.println("\n___DECORATOR PATTERN___\n"); 
        
        VegFood vf=new VegFood();  
        System.out.println(vf.prepareFood());  
        System.out.println( vf.foodPrice());  

        Food f1=new NonVegFood((Food) new VegFood());  
        System.out.println(f1.prepareFood());  
        System.out.println( f1.foodPrice());  

        Food f2=new ChineeseFood(f1);  
        System.out.println(f2.prepareFood());  
        System.out.println( f2.foodPrice()); 
        
        //Iterator Pattern
        System.out.println("\n___ITERATOR PATTERN___\n"); 
        
        CollectionofNames names = new CollectionofNames();  

        for(Iterator iter = names.getIterator(); iter.hasNext();){  
            String name = (String)iter.next();  
            System.out.println("Name : " + name);  
        }  
    }
}
