/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Factory;

/**
 *
 * @author SUNNY
 */
public class GetPlanFactory {
    public Plan getPlan(String planName){
        if(planName == null){
            return null;
        }
        else if(planName.equals("domestic")){
            return new DomesticPlan();
        }
        else if(planName.equals("commercial")){
            return new CommercialPlan();
        }
        else if(planName.equals("institutional")){
            return new InstitutionalPlan();
        }
        else{
            return null;
        }
    }
}
