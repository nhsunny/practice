/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Adapter;

/**
 *
 * @author SUNNY
 */
public class BankCustomer extends BankDetails implements CreditCard{

    @Override
    public void giveBankDetails() {
        setAccNumber(1);
        setAccHolderName("Sunny");
        setBankName("ICICI");
    }

    @Override
    public String getCreditCard() {
        return "Credit Card is valid for "+getAccHolderName()+" with account no. "+getAccNumber()+" in "+getBankName()+" bank";
    }
    
}
