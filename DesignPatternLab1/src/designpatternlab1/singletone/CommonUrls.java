
package designpatternlab1.singletone;

public class CommonUrls {
    public static CommonUrls commonUrls;
    
    public static CommonUrls getInstance(){
        if(commonUrls == null)
            commonUrls = new CommonUrls();
        return commonUrls;
    }
    
    public String name = "Hello";
}
