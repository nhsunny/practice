package designpatternlab1;

import designpatternlab1.factorypattern.Bank;
import designpatternlab1.factorypattern.Factory;
import designpatternlab1.singletone.CommonUrls;

public class DesignPatternLab1 {
    public static void main(String[] args) {
        // singletone design pattern
        System.out.println(CommonUrls.getInstance().name);
        
        // factory design pattern
        Factory factory = new Factory();
        Bank b = factory.getBankInformation("DBBL");
        System.out.println(b.getBankName());
    }
}
